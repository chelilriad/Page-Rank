  #!/bin/bash
  #@ class            = clallmds                  
  #@ job_name         = log                    
  ##@ total_tasks      = 1                          
  ##@ node             = 1                         
  #@ wall_clock_limit = 00:15:00                   
  #@ output           = $(job_name).$(jobid).log  
  #@ error            = $(job_name).$(jobid).err   
  #@ job_type         = serial                     
  #@ queue                                        
  #
  module load atlas
  module load gnu-env/5.4.0                            # By default no environment propagated 
  #                                                # So it is necessary to position it via one of the three methods:
  #                                                #   Usage of module 
  #                                                #   Adding the submission directive #@ environment = COPY_ALL
  #                                                #   Editing file ~/.bash_profile
  #                                                # Conflicts might occur if a combination of these means are used
  ../PageRank 1 ../../matrix_Row/power.mtx 