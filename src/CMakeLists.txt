cmake_minimum_required (VERSION 2.6 FATAL_ERROR)

IF (${CMAKE_MAJOR_VERSION} EQUAL 2 AND ${CMAKE_MINOR_VERSION} LESS 9 AND ${CMAKE_PATCH_VERSION} LESS 12)
    message(STATUS "using 2.6.0+ version of the CMakeLists.txt")
    enable_language(Fortran)
    SET(GCC_COVERAGE_COMPILE_FLAGS "-std=c++11 -O3 -Wall")
ELSEIF (${CMAKE_MAJOR_VERSION} EQUAL 3 AND ${CMAKE_MINOR_VERSION} GREATER 0)
    message(STATUS "using 3.1+ version of the CMakeLists.txt ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}.${CMAKE_PATCH_VERSION}")
    set(CMAKE_CXX_STANDARD 11)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)
    set(CMAKE_CXX_EXTENSIONS OFF) 
    SET(GCC_COVERAGE_COMPILE_FLAGS "-O3 -Wall")
ELSE (${CMAKE_MAJOR_VERSION} EQUAL 2 AND ${CMAKE_MINOR_VERSION} LESS 9 AND ${CMAKE_PATCH_VERSION} LESS 12)
    message(STATUS "using 2.8.12+ version of the CMakeLists.txt")
    add_compile_options(-std=c++11)
    SET(GCC_COVERAGE_COMPILE_FLAGS "-O3 -Wall")
ENDIF (${CMAKE_MAJOR_VERSION} EQUAL 2 AND ${CMAKE_MINOR_VERSION} LESS 9 AND ${CMAKE_PATCH_VERSION} LESS 12)


project (PageRank)
project (Matrix_Converter)
#Delta
if(NOT DELTA_VALUE)
    set (DELTA_VALUE 0.85)
ENDIF (NOT DELTA_VALUE)
#Tol
if(NOT TOL_VALUE)
    set (TOL_VALUE 1e-14)
ENDIF (NOT TOL_VALUE)
if(NOT MAX_ITE_VALUE)
    set (MAX_ITE_VALUE 200)
ENDIF (NOT MAX_ITE_VALUE)

#find BLAS
IF(NOT BLAS_LIBS)
    find_package( BLAS REQUIRED )
    include_directories(${BLAS_INCLUDE_DIR})
ENDIF (NOT BLAS_LIBS)


SET( CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}" )

include_directories("${PROJECT_SOURCE_DIR}/Bin")
# configure a header file to pass some of the CMake settings to the source code
configure_file (
  "${PROJECT_SOURCE_DIR}/Bin/Config.hpp.in"
  "${PROJECT_SOURCE_DIR}/Bin/Config.hpp"
  )
  

#Add PowerMethod Library
include_directories ("${PROJECT_SOURCE_DIR}/PowerM")
add_subdirectory (PowerM)
set (EXTRA_LIBS ${EXTRA_LIBS} PowerMethod)

#Add Matrix Market Library
include_directories ("${PROJECT_SOURCE_DIR}/Matrix_Market")
add_subdirectory (Matrix_Market)
set (EXTRA_LIBS ${EXTRA_LIBS} MatrixMarket)

#Link BLAS
IF(NOT BLAS_LIBS)
    set (EXTRA_LIBS ${EXTRA_LIBS} blas ${BLAS_LIBRARIES})
ELSE (NOT BLAS_LIBS)
set (EXTRA_LIBS ${EXTRA_LIBS} ${BLAS_LIBS})
ENDIF (NOT BLAS_LIBS)

add_executable(PageRank main.cpp)
target_link_libraries (PageRank ${EXTRA_LIBS})

#set(MPI_C_COMPILER=/home/user/mpich-3.2/bin/mpicc)
find_package(MPI REQUIRED)
include_directories(${MPI_INCLUDE_PATH})
set (EXTRA_LIBS ${EXTRA_LIBS} ${MPI_LIBRARIES})

find_package(OpenMP REQUIRED)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS} -Wall -Werror ")


add_executable(Matrix_Converter Matrix_Converter.cpp)
target_link_libraries (Matrix_Converter ${EXTRA_LIBS})