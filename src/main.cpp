using namespace std;
#include <stdlib.h> 
//#include <unistd.h> //Used to pause at various moments for debuging
#include <time.h>  //  Used for the Random Seed
#include "PowerM/PowerMethod.hpp"
#include "./Matrix_Market/matrix_market_read.hpp"
#include <chrono>

void damping(matSPRS& A);

#define BLAS 1

int main (int argc, char * argv[])
{	int CSR;
	auto start = chrono::high_resolution_clock::now();
	auto starttot = chrono::high_resolution_clock::now();
	if(argc==2)
	{
		CSR=atoi(argv[1]);
	}
	else if(argc==3)
	{
		cout<<argv[2]<<endl;
		CSR=atoi(argv[1]);
		cout<<CSR<<endl;
	}
	else
	{
		cout<<"Aucun ou trop d'argument, utilisation des valeurs par défaut.\nUtilisation de CSR, et de la matrice d'exemple\n";
		CSR=1;
	}
	matSPRS A;
	if(argc==3)
	{
		if(CSR!=0)
		{
			if(CSR==2)
			{
				A=readMarketMatrixCSR(argv[2]);
			}
			else
			{
				A=readMarketMatrixRowCSR(argv[2]);
			}
			
		}
		else
		{
			A=readMarketMatrixCOO(argv[2]);
		}
	}
	else
	{
		if(CSR!=0)
		{
			A.Val={2,1,1,1};
			A.Col={2,0,0,1,3}; //CSR example from the Subject example
			A.Row={0,1,2,5,5};
		}
		else
		{
			A.Val={2,1,1,1};
			A.Col={2,0,0,1,3}; //COO example from the Subject example
			A.Row={0,1,2,2,2};
		}
	}
	const int SIZE=A.Val.size();
	auto end = chrono::high_resolution_clock::now();
	auto tio=end-start;
	start = chrono::high_resolution_clock::now();
	srand(7); //Random Seed
	double eigVect[SIZE];
	for(int i=0;i<SIZE;i++)
	{
		eigVect[i]=static_cast <double>(rand()%100+1); //Random Vector to start the Power Method with
		if(argc<3)
		{
			if(i==0) cout<<"Vecteur Aléatoire Généré pour la Power Methode :\n";
			cout<<eigVect[i]<<endl;
		}
	}

	cout<<"\nNormalisation de la matrice avec le damping (DELTA)\n";
	damping(A);
	end = chrono::high_resolution_clock::now();
	auto tdamp=end-start;
	start = chrono::high_resolution_clock::now();
	double eigenValue;
	if(CSR!=0)
	{
		cout<<"\nAppel de la Power Methode CSR\n";
		eigenValue = powerMethodeCSR(A,SIZE,eigVect, MAX_ITE);
	}
	else
	{
		cout<<"\nAppel de la Power Methode COO\n";
		eigenValue = powerMethodeCOORM(A,SIZE,eigVect, MAX_ITE); 	
	}
	end = chrono::high_resolution_clock::now();
	auto tpower=end-start;
	start = chrono::high_resolution_clock::now();
	cout<<"Plus grande Valeur Propre: "<<eigenValue<<endl;
	if(argc<3)
	{
		cout<<"Vecteur Propre associé :\n";
		for(int i=0;i<SIZE;i++)
		{
			 cout<<eigVect[i]<<"\n";
		}
	cout<<"Ce Vecteur représente les valeurs du PageRank non normalisé\n";
	}
	if(argc<2)
	{
		cout<<"\nVérification A*b=b avec A la matrice d'adjacence et b le vecteur propre généré\n";
		double tmp[SIZE];
		int colDone[SIZE];
		for(int i=0; i<SIZE; i++) 
		{
			tmp[i]=0;       
			fill_n(colDone,SIZE,0);
			for (int j=A.Row[i]; j<A.Row[i+1]; j++) 
			{
				//sparse dot product
				tmp[i] += A.Val[A.Col[j]]*eigVect[A.Col[j]];
				colDone[A.Col[j]]=1;
			}
			for(int j=0;j<SIZE;j++)
			{
				if(colDone[j]==0) //If the Dot product didn't go through this value
				{ 
					if(A.Val[j]==(1./SIZE)) //If this column had no '1' initially
					{
						tmp[i]+=A.Val[j]*eigVect[j];
					}
					else
					{
						tmp[i]+=((1.-DELTA)/SIZE)*eigVect[j];
					}
				}
			}
			cout<<tmp[i] << " =? " << eigVect[i] <<"\n";
		}
	}
	
	cout<<"\nNormalisation du PageRank autour de la valeur propre :"<<endl;
	double normal=eigenValue*SIZE/cblas_dasum(SIZE,eigVect,1); 
	cblas_dscal(SIZE,normal,eigVect,1); //Each member of eigVect is divided by the sum of each member divided by the size time the eigenvalue
	normal=cblas_dasum(SIZE,eigVect,1);
	double min=99999,max=0,val;
	for(int i=0;i<SIZE;i++)
	{
		val=eigVect[i];
		//cout<<val<<" ";
		if(i==0)
		{
			min=val;
			max=val;
		}
		else
		{
			if(val<min)
			{
				min=val;
			}
			else if(val>max)
			{
				max=val;
			}
		}
	}
	end = chrono::high_resolution_clock::now();
	auto endtot = chrono::high_resolution_clock::now();
	auto tnorm=end-start;
	fprintf(stdout,"Taille:%d\n",SIZE);
	fprintf(stdout,"Tache (seq):%d\n",1);
	fprintf(stdout,"Durée IO:%.10g\n",(chrono::duration_cast<chrono::microseconds>(tio).count()/1000000.));
	fprintf(stdout,"Durée Damping:%.10g\n",(chrono::duration_cast<chrono::microseconds>(tdamp).count()/1000000.));
	fprintf(stdout,"Durée PowerMethod:%.10g\n",(chrono::duration_cast<chrono::microseconds>(tpower).count()/1000000.));
	fprintf(stdout,"Durée Norme:%.10g\n",(chrono::duration_cast<chrono::microseconds>(tnorm).count()/1000000.));
	fprintf(stdout,"Durée Total:%.10g\n",(chrono::duration_cast<chrono::microseconds>(endtot-starttot).count()/1000000.));
	fprintf(stdout,"\nmoyenne:%g\n",normal/SIZE);
	fprintf(stdout,"minimum:%g\n",min);
	fprintf(stdout,"maximum:%g\n",max);
	fprintf(stdout,"Delta used : %g  Tolerance used : %g\n",DELTA,TOL);
}


void damping(matSPRS& A)
{
	const int SIZE=A.Val.size();
	double val;
	matSPRS B=A;
	const double colEmpty=(1./SIZE), zeroValue=((1.-DELTA)/SIZE);
	for(int c=0;c<SIZE;c++)
	{
		val=A.Val[c];
		
		if(val!=0.) //if there is a link going out of this column
		{
			B.Val[c]=(DELTA*(1./val)+zeroValue); //Val=Delta*(1/number of outgoing links)+(1-Delta)/Size of matrix
		}
		else //If the column has no outgoing links
		{
			B.Val[c]=colEmpty;//Val=1/Size of matrix 
		}
		
	}
	A=B;
}

